<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/currencies', 'CurrencyController@index')->name('currencies_list');
Route::get('/currencies/add', 'CurrencyController@add')->name('add_currency');
Route::post('/currencies/{id?}', 'CurrencyController@store')->name('store_currency');
Route::get('/currencies/{id}', 'CurrencyController@show');
Route::get('/currencies/{id}/edit', 'CurrencyController@edit');
Route::delete('/currencies/{id}/destroy', 'CurrencyController@destroy')->name('delete_currency');
