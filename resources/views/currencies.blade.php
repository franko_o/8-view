@extends('layouts.frontend')

@section('header')
    @include('parts.header')
@stop

@section('content')

    <h2 class="content-title">Currency market</h2>

    <div class="row">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"></th>
                <th scope="col">Name</th>
                <th scope="col">Short Name</th>
                <th scope="col">Price</th>
                <th scope="col">Handle</th>
            </tr>
            </thead>
            <tbody>
                @foreach($currencies as $currency)
                    <tr>
                        <td scope="row">{{$currency->id}}</td>
                        <td><img src="{{$currency->logo_url}}"></td>
                        <td><a class="nav-link" href="/currencies/{{$currency->id}}">{{$currency->title}}</a></td>
                        <td>{{$currency->short_name}}</td>
                        <td>{{$currency->price}}</td>
                        <td>
                            @include('currency.actions')
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>

@stop

@section('footer')
    @include('parts.footer')
@stop