@extends('layouts.frontend')

@section('header')
    @include('parts.header')
@stop

@section('content')

    <div class="container">

        <h2 class="content-title">Edit {{ $currency->title }}</h2>

        <div class="row">
            <div class="col-sm-12">
                <form id="addform" action="{{route('store_currency', ['id' => $currency->id])}}" method="post">
                    {{ csrf_field() }}

                    <input type="hidden" name="id" id="id" value="{{ $currency->id }}" readonly>
                    <div class="form-group">
                        <label for="title">Name</label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Enter name" value="@if(old('title')){{old('title')}}@else{{ $currency->title }}@endif">
                        @if(isset($errors))
                            <small id="titleHelp" class="form-text text-error">{{ $errors->first('title') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="short_name">Short Name</label>
                        <input type="text" class="form-control" name="short_name" id="short_name" value="@if(old('short_name')){{old('short_name')}}@else{{ $currency->short_name }}@endif" placeholder="Enter short name">
                        @if(isset($errors))
                            <small id="short_nameHelp" class="form-text text-error">{{ $errors->first('short_name') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="logo_url">Image URI</label>
                        <input type="text" class="form-control" name="logo_url" id="logo_url" value="@if(old('logo_url')){{old('logo_url')}}@else{{ $currency->logo_url }}@endif" placeholder="Enter URI">
                        @if(isset($errors))
                            <small id="logo_urlHelp" class="form-text text-error">{{ $errors->first('logo_url') }}</small>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" class="form-control" name="price" id="price" value="@if(old('price')){{old('price')}}@else{{ $currency->price }}@endif" placeholder="Enter price">
                        @if(isset($errors))
                            <small id="priceHelp" class="form-text text-error">{{ $errors->first('price') }}</small>
                        @endif
                    </div>

                    <input class="btn btn-success" type="submit" value="Save">
                </form>
            </div>
        </div>

        @include('additional.backtolist')
    </div>

@stop

@section('footer')
    @include('parts.footer')
@stop