<a type="button"
   class="btn btn-info edit-button"
   href="{{action('CurrencyController@edit', $currency->id)}}">Edit currency</a>

<form class="delete_currency" action="{{route('delete_currency', ['id' => $currency->id])}}" method="post">
    {{ method_field('delete') }}
    {{ csrf_field() }}
    <button type="submit" class="btn btn-danger delete-button"><span>Delete</span></button>
</form>