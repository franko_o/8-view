<a class="nav-link" href="{{ route('currencies_list') }}">Currencies</a>
<a class="nav-link green" href="{{ route('add_currency') }}">Add</a>

@if (! empty($currencies))
    @foreach($currencies as $currency)
        <a class="nav-link" href="/currencies/{{$currency->id}}">{{$currency->title}}</a>
    @endforeach
@endif
