<?php

namespace App\Http\Controllers;

use App\Http\Requests\CurrencyRequest;
use App\Currency;

class CurrencyController extends Controller
{
    public function index()
    {
        $currencies = Currency::all();
        $title = 'Currency market';
        if ($currencies->count() == 0) {
            return view('nocurrencies', compact('title'));
        }
        return view('currencies', compact('title', 'currencies'));
    }

    public function show($id)
    {
        $currencies = Currency::all();

        $currency = Currency::find($id);
        if (!$currency) {
            abort(404);
        }
        $title = $currency->title;

        return view('currency.view', compact('title', 'currency', 'currencies'));
    }

    public function add()
    {
        $currencies = Currency::all();
        $title = 'Add currency';
        return view('currency.new', compact('title', 'currencies'));
    }

    public function edit($id)
    {
        $currencies = Currency::all();

        $currency = Currency::find($id);
        if (!$currency) {
            abort(404);
        }
        $title = $currency->title;

        return view('currency.edit', compact('title', 'currency', 'currencies'));
    }

    public function store(CurrencyRequest $request, $id = null)
    {
        $isEdit = isset($id);

        $model = $isEdit ? Currency::findOrFail($request->id) : new Currency();
        $model->title = $request->title;
        $model->short_name = $request->short_name;
        $model->logo_url = $request->logo_url;
        $model->price = $request->price;
        $model->save();

        return $isEdit ? redirect('/currencies/' . $model->id) : redirect('/currencies/');
    }

    public function destroy($id)
    {
        $model = Currency::findOrFail($id);
        $model->delete();
        return redirect('currencies');
    }
}
